FROM python:3.4
ENV PYTHONUNBUFFERED 1
ENV WITH_DOCKER True

RUN mkdir /config
ADD /config/requirements.pip /config/

RUN pip install -r /config/requirements.pip

RUN mkdir /code
WORKDIR /code
ADD /imageboard/. /code/

RUN adduser --disabled-password --gecos '' myuser
