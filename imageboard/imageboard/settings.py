import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'static')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

ALLOWED_HOSTS = ['web','localhost','127.0.0.1','188.225.36.114']

SECRET_KEY = os.urandom(32)

DEBUG = True

# Application definition

INSTALLED_APPS = [
    # whitenoise on the top of staticfiles
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    # Django
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    # Third party apps
    'rest_framework',
    'rest_framework.authtoken',
    'taggit',
    'corsheaders',
    'drf_multiple_model',
    # Internal apps
    'board',
    'finalware',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
]

ROOT_URLCONF = 'imageboard.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'imageboard.wsgi.application'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'imageboard',
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = '/static/'


CORS_ORIGIN_WHITELIST = (
    'localhost:8000',
    'localhost:4200',
    '127.0.0.1:8000',
    '127.0.0.1:4200',
    '188.225.36.114:4200',
    '188.225.36.114:8000',
    'web:8000',
    'web:4200',
)

SITE_OBJECTS_INFO_DICT = {
    '1': {
        'name': 'production',
        'domain': '188.225.36.114:8000'
    },
    '2':{
        'name': 'integration',
        'domain': 'example.org'
    },
    '3':{
        'name': 'development',
        'domain': '127.0.0.1:8000'
    },
}
SITE_ID = 1

MEDIA_ROOT = os.path.join(BASE_DIR, 'uploaded_media')
MEDIA_URL = '/media/'

#bootstrap

SITE_SUPERUSER_USERNAME = 'valentin'

SITE_SUPERUSER_EMAIL = 'valenteenium@gmail.com'

SITE_SUPERUSER_PASSWORD = 'vpassword'

# Imageboard settings

PREVIEW_LENGTH = 3
MAX_IMAGES = 4
BUMPLIMIT = 500
THREADLIMIT = 50

if os.environ.get('WITH_DOCKER', False) == 'True': from .docker_settings import *
