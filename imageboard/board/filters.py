def joined_search_filter(queryset, request, *args, **kwargs):

    parameters=['body__icontains','tags__name']

    query = {p:request.query_params[p] for p in parameters if p in request.query_params.keys()}

    return queryset.filter(**query)
