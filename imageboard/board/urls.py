from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()

router.register(r'boards', views.BoardViewSet)
router.register(r'images', views.ImagesViewSet)
router.register(r'threads', views.ThreadViewSet)
router.register(r'posts', views.PostViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'search', views.SearchViewSet, base_name='search')
