from django.db.models import Q
from django.conf import settings
from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Post, Thread, Board, Image


User = get_user_model()


class ImageSerializer(serializers.ModelSerializer):
    links = serializers.SerializerMethodField()
    created = serializers.ReadOnlyField()

    class Meta:
        model = Image
        fields = ('pk', 'created', 'image', 'thumbnail', 'links')
        read_only_fields = ('thumbnail',)

    def get_links(self, obj):
        request = self.context['request']

        #generating link to picture's thread
        #but we are not sure if it's mom is thread or post
        links={}

        mother = obj.common
        try:
            post = Post.objects.get(pk=mother.id)
            grandmother = post.mother_thread
            links['thread'] = reverse('thread-detail',\
                kwargs={'pk': grandmother.id}, request=request)+'full/#'+str(mother.id)
        except:
            #Post.objects.get failure means mom is a thread
            links['thread'] = reverse('thread-detail',\
                kwargs={'pk': mother.id}, request=request)+'full/'

        return links


class CommonMixin(object):
    references = serializers.ReadOnlyField()

    def create(self, validated_data):

        if 'images' in validated_data.keys():
            images_data = validated_data.pop('images')
        else:
            images_data = []

        envelope = self.Meta.model.objects.create(**validated_data)

        for image_data in images_data:
            Image.objects.create(common=envelope, **image_data)
        return envelope

    def get_tags(self, obj):
        return obj.tags.names()

    def get_links(self, obj):
        request = self.context['request']

        #references are humans-written '>>id' links
        #transforming into real URLs by this serializer
        refs=[int(x) for x in obj.references.split(',') if len(x)>0]
        ref_links={}

        # -_-
        #
        #should have made post/thread one type of entity

        posts = Post.objects.filter(id__in=refs)
        for p in posts:
            ref_links[p.id]=reverse('post-detail',
                kwargs={'pk': p.id}, request=request)

        threads = Thread.objects.filter(id__in=refs)
        for t in threads:
            ref_links[t.id]=reverse('thread-detail',
                kwargs={'pk': t.id}, request=request)

        #tags are #this #kind #of words, we gonna make links\
        #to entities with same tags
        tag_links={key:reverse('search-list',request=request)+'?tags__name='+key\
            for key in obj.tags.names()}

        return {'references':ref_links, 'tag_links':tag_links}


class PostSerializer(CommonMixin, serializers.ModelSerializer):
    created = serializers.ReadOnlyField()
    links = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    images = ImageSerializer(many=True,required=False)

    class Meta:
        model = Post
        fields = ('body', 'created', 'mother_thread', 'id', 'links',\
                    'tags', 'images')

    def get_links(self, obj):
        request = self.context['request']
        links=super(PostSerializer, self).get_links(obj)

        links.update( {
            'self': reverse('post-detail',
                kwargs={'pk': obj.pk}, request=request),
            'mother_thread_id': reverse('thread-detail',
                kwargs={'pk': obj.mother_thread_id}, request=request),
        } )

        return links

class ThreadSerializer(CommonMixin, serializers.ModelSerializer):
    """
        basic version
    """
    created = serializers.ReadOnlyField()
    bumped = serializers.DateTimeField(read_only=True)
    links = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    images = ImageSerializer(many=True)

    class Meta:
        model = Thread
        fields = ('body', 'created', 'board', 'id', 'bumped', \
            'nposts', 'links', 'tags', 'images')

    def get_links(self, obj):
        request = self.context['request']
        links=super(ThreadSerializer, self).get_links(obj)

        links.update({
            'self': reverse('thread-detail',
                kwargs={'pk': obj.pk}, request=request)+'full/',
            'board': reverse('board-detail',
                kwargs={'pk': obj.board_id}, request=request),
            'posts': reverse('post-list',
                request=request) + '?mother_thread={}'.format(obj.pk),
        })

        return links


class PreviewThreadSerializer(ThreadSerializer):
    """
        thread + few last comments
    """

    recent_posts = serializers.SerializerMethodField()

    class Meta(ThreadSerializer.Meta):
        fields=ThreadSerializer.Meta.fields + ('recent_posts',)

    def get_recent_posts(self,object):
        posts = Post.objects.filter(mother_thread=object)\
            .order_by('-created')[:settings.PREVIEW_LENGTH][::-1]
        serializer = PostSerializer(context=\
            {'request': self.context['request']}, instance=posts,\
            many=True, read_only=True)
        return serializer.data


class FullThreadSerializer(ThreadSerializer):
    """
       thread + all its comments
    """

    posts = serializers.SerializerMethodField()

    class Meta(ThreadSerializer.Meta):
        fields=ThreadSerializer.Meta.fields + ('posts',)

    def get_posts(self,object):
        posts_data = Post.objects.filter(mother_thread=object)\
            .order_by('created')
        serializer = PostSerializer(context=\
            {'request': self.context['request']}, instance=posts_data,\
                many=True, read_only=True)
        return serializer.data


class BoardSerializer(serializers.ModelSerializer):
    links = serializers.SerializerMethodField()
    bump_limit = serializers.IntegerField(required=False)
    thread_limit = serializers.IntegerField(required=False)

    class Meta:
        model = Board
        fields = ('name', 'description', 'nthreads', 'links', 'bump_limit', 'thread_limit')

    def get_links(self, obj):
        request = self.context['request']

        #includes link to thread list with "preview" argument
        #(look in ThreadViewSet for details)
        return {
            'self': reverse('board-detail',
                kwargs={'pk': obj.pk}, request=request),
            'threads': reverse('thread-list',
                request=request) + 'preview/?board={}'.format(obj.pk),
        }


class UserSerializer(serializers.ModelSerializer):

    full_name = serializers.CharField(source='get_full_name', read_only=True)

    class Meta:
        model = User
        fields = ('id', User.USERNAME_FIELD, 'full_name', 'is_active', )
