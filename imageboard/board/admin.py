from django.contrib import admin

from .models import Post, Thread, Board


admin.site.register(Post)
admin.site.register(Thread)
admin.site.register(Board)
