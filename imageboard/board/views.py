from django.contrib.auth import get_user_model
from rest_framework import authentication, permissions, viewsets, filters, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from drf_multiple_model.viewsets import MultipleModelAPIViewSet
from drf_multiple_model.mixins import Query

from .models import Post, Thread, Board, Image
from .serializers import BoardSerializer, ThreadSerializer, PostSerializer, \
    PreviewThreadSerializer, ImageSerializer, FullThreadSerializer, UserSerializer
from .forms import ThreadFilter, PostFilter
from .parsers import MultipartFormencodeParser
from .filters import joined_search_filter


User = get_user_model()


class PaginationParams(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

    def get_paginated_response(self, data):
        return Response({
            'links': {
               'next': self.get_next_link(),
               'previous': self.get_previous_link()
            },
            'item_count': self.page.paginator.count,
            'page_count': self.page.paginator.num_pages,
            'results': data
        })


class DefaultsMixin(object):
    """Default settings for view authentication, permissions,
    filtering and pagination"""

    filter_backends =(
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )

    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )

class SecurityMixin(object):
    authentication_classes = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication,
    )

    permission_classes = (
        permissions.IsAuthenticated,
    )

class BoardViewSet(DefaultsMixin, viewsets.ModelViewSet):
    queryset = Board.objects.order_by('name')
    serializer_class = BoardSerializer

    def get_permissions(self):
        if self.action in ['create','update','partial_update','destroy']:
            self.permission_classes = [permissions.IsAuthenticated,]
        else :
            self.permission_classes = [permissions.AllowAny,]
        return super(self.__class__, self).get_permissions()


class ThreadViewSet(DefaultsMixin, viewsets.ModelViewSet):
    pagination_class = PaginationParams
    queryset = Thread.objects.order_by('-bumped')
    serializer_class = ThreadSerializer
    filter_class = ThreadFilter
    parser_classes = (MultipartFormencodeParser,)

    def get_permissions(self):
        if self.action in ['update','partial_update','destroy']:
            self.permission_classes = [permissions.IsAuthenticated,]
        else :
            self.permission_classes = [permissions.AllowAny,]
        return super(self.__class__, self).get_permissions()


    @list_route()
    def preview(self, *args, **kwargs):
        self.serializer_class = PreviewThreadSerializer
        return self.list(self,  *args, **kwargs)

    @detail_route(methods=['get'])
    def full(self, *args, **kwargs):
        self.serializer_class = FullThreadSerializer
        return self.retrieve(self,  *args, **kwargs)


class PostViewSet(DefaultsMixin, viewsets.ModelViewSet):
    queryset = Post.objects.order_by('created')
    serializer_class = PostSerializer
    parser_classes = (MultipartFormencodeParser,)

    def get_permissions(self):
        if self.action in ['update','partial_update','destroy']:
            self.permission_classes = [permissions.IsAuthenticated,]
        else :
            self.permission_classes = [permissions.AllowAny,]
        return super(self.__class__, self).get_permissions()


class ImagesViewSet(DefaultsMixin, viewsets.ReadOnlyModelViewSet):
    """gonna need this for jumbotron with thread links on starting page"""
    pagination_class = PaginationParams

    queryset = Image.objects.order_by('-created')
    serializer_class = ImageSerializer


class UserViewSet(SecurityMixin, DefaultsMixin, viewsets.ReadOnlyModelViewSet):

    lookup_field = User.USERNAME_FIELD
    lookup_url_kwarg = User.USERNAME_FIELD
    queryset = User.objects.order_by(User.USERNAME_FIELD)
    serializer_class = UserSerializer
    search_fields = (User.USERNAME_FIELD, )


class SearchViewSet(MultipleModelAPIViewSet):
    """search through posts and threads"""

    flat = True
    add_model_type = True
    sorting_field = '-created'

    filter_backends =(
        filters.SearchFilter,
    )

    queryList = [
        Query(Post.objects.all(),PostSerializer, filter_fn=joined_search_filter),
        Query(Thread.objects.all(),ThreadSerializer, filter_fn=joined_search_filter),
        # (Poem.objects.filter(style='Sonnet'),PoemSerializer),
    ]
