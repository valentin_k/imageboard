import datetime
import os
import uuid

from django.db import models
from django.core import validators
from django.conf import settings

from PIL import Image as Img
from taggit.managers import TaggableManager


def scramble_uploaded_filename(instance, filename):
    """
    Scramble / uglify the filename of the  file, but keep the files extension (e.g., .jpg or .png)
    """
    extension = filename.split(".")[-1]
    return "{}.{}".format(uuid.uuid4(), extension)


def create_thumbnail(input_image, thumbnail_size=(256, 256)):
    """
    Create a thumbnail of an existing image
    """
    # make sure an image has been set
    if not input_image or input_image == "":
        return

    # open image
    image = Img.open(input_image)

    # use PILs thumbnail method; use anti aliasing to make the scaled picture look good
    image.thumbnail(thumbnail_size, Img.ANTIALIAS)

    # parse the filename and scramble it
    filename = scramble_uploaded_filename(None, os.path.basename(input_image.name))
    arrdata = filename.split(".")
    # extension is in the last element, pop it
    extension = arrdata.pop()
    basename = "".join(arrdata)
    # add _thumb to the filename
    new_filename = basename + "_thumb." + extension
    #new_filename = 'Engeneer.jpg'

    # save the image in MEDIA_ROOT and return the filename
    image.save(os.path.join(settings.MEDIA_ROOT, new_filename))

    return new_filename

class Image(models.Model):
    #stolen
    """
    Provides a Model which contains an  image aswell as a thumbnail
    """
    image = models.ImageField("Uploaded image", \
        upload_to=scramble_uploaded_filename)

    # thumbnail
    thumbnail = models.ImageField("Thumbnail of  image", blank=True)

    # image's parent
    common = models.ForeignKey('Common', null=True, related_name='images', \
        on_delete=models.CASCADE)

    created = models.DateTimeField(null=True)

    def __str__(self):
        return 'image # ' + str(self.id)

    def save(self, force_insert=False, force_update=False, \
            using=None, update_fields=None):
        """
        On save, generate a new thumbnail
        """
        # generate and set thumbnail or none
        self.thumbnail = create_thumbnail(self.image)

        # Check if a pk has been set, meaning that we are not creating a new image, but updateing an existing one
        if self.pk:
            force_update = True

        self.created = datetime.datetime.now()

        # force update as we just changed something
        super(Image, self).save(force_update=force_update)

    def delete(self, *args, **kwargs):
        os.remove((os.path.join(settings.MEDIA_ROOT, str(self.thumbnail))))
        os.remove((os.path.join(settings.MEDIA_ROOT, str(self.image))))
        super(Image, self).delete(*args, **kwargs)

class Common(models.Model):
    """Post and Thread have a lot in common """

    body = models.TextField(null=True, blank=True, max_length=1000)
    created = models.DateTimeField()  #blank=True, null=True
    tags = TaggableManager()
    references = models.CharField(max_length=1024,validators=\
        [validators.validate_comma_separated_integer_list])

    def save(self, *args, **kwargs):
        self.created = datetime.datetime.now()

        #tag and refs check
        raw=self.body.split(' ') #! we forgot about '\n'

        #some text with >>777 referenes to threads/posts
        reference_cloud = [x[2:] for x in\
            filter(lambda z: len(z)>3 and\
                z[:2]=='>>' and z[2:].isdigit() ,raw)]

        self.references=','.join(reference_cloud)

        super(Common, self).save(*args, **kwargs)

        #some text with #tags
        tag_cloud = [x[1:] for x in filter(lambda z: len(z)>2 and z[0]=='#',raw)]
        for tag in tag_cloud:
            self.tags.add(tag)

    def delete(self, *args, **kwargs):
        #delete corresponding pictures
        disposal_appointed=self.images.all()
        for item in disposal_appointed:
            item.delete()
        super(Common, self).delete(*args, **kwargs)

    class meta:
        abstract = True


class Post(Common):
    mother_thread = models.ForeignKey('Thread', related_name='ancestors')

    def save(self, *args, **kwargs):
        #moving Thread with new message to the top
        mom=self.mother_thread
        mom.bump()
        mom.save()
        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return 'post №' + str(self.id)


class Thread(Common):
    board = models.ForeignKey('Board', related_name='threads', \
        on_delete=models.CASCADE)
    bumped = models.DateTimeField(blank=True, null=True)

    @property
    def nposts(self):
        return self.ancestors.count()

    def bump(self):
        #...but threads cant live forever
        if self.ancestors.count() < self.board.bump_limit:
            self.bumped = datetime.datetime.now()

    def save(self, *args, **kwargs):
        self.bumped = datetime.datetime.now()
        super(Thread, self).save(*args, **kwargs)

        #deleting old Thread
        current_board_threads=self.board.threads.order_by('bumped')
        if current_board_threads.count() > self.board.thread_limit:
            current_board_threads[0].delete()

    def __str__(self):
        return 'thread №' + str(self.id)

    def delete(self, *args, **kwargs):
        #deleting thread's messages
        disposal_appointed=self.ancestors.all()
        for item in disposal_appointed:
            item.delete()
        super(Thread, self).delete(*args, **kwargs)

class Board(models.Model):
    name = models.CharField(max_length=10, primary_key=True, default='')
    description = models.TextField(blank=True, default='')

    thread_limit = models.PositiveIntegerField(default=settings.THREADLIMIT)
    bump_limit = models.PositiveIntegerField(default=settings.BUMPLIMIT)

    @property
    def nthreads(self):
        return self.threads.count()

    def __str__(self):
        return self.name

    def delete(self, *args, **kwargs):
        #deleting board's threads
        disposal_appointed=self.threads.all()
        for item in disposal_appointed:
            item.delete()
        super(Board, self).delete(*args, **kwargs)
