from formencode.variabledecode import variable_decode

from django.conf import settings

from rest_framework import parsers


class MultipartFormencodeParser(parsers.MultiPartParser):

    def parse(self, stream, media_type=None, parser_context=None):
        result = super().parse(
            stream,
            media_type=media_type,
            parser_context=parser_context
        )

        data = variable_decode(result.data)
        d2=dict(result.files)
        
        if 'images' in d2.keys():
            data['images']=[{'image':img} for img in d2['images'][:settings.MAX_IMAGES]]

        return data
