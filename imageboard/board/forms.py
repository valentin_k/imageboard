import django_filters
from django.contrib.auth import get_user_model
from django.db import models

from .models import Post, Thread, Board


# class TagsFilter(django_filters.BaseFilterBackend):
#     """
#     Return all objects which match any of the provided tags
#     """
#
#     def filter_queryset(self, request, queryset, view):
#         tags = request.query_params.get('tags', None)
#         if tags:
#             tags = tags.split(',')
#             queryset = queryset.filter(tags__name__in=tags).distinct()
#
#         return queryset

class ThreadFilter(django_filters.FilterSet):

    # def filter_tags(self, queryset, name, value):
    #     return queryset.filter(body__isnull=False)

    class Meta:
        model = Thread
        fields = ('board', 'created')  # , 'tags'

class PostFilter(django_filters.FilterSet):

    class Meta:
        model = Post
        fields = ('mother_thread', 'created', 'body')
