# README #

Django-Rest based imageboard forum.
https://en.wikipedia.org/wiki/Imageboard

Better use scripts from tests folder instead of ticky-tacky front-end

http://188.225.84.209:8000/api/ - this project on vds


## What is this repository for? ##

self-education / portfolio

## short api discription:##

    /boards/ - forum sections

    /threads/ - basic threads view

      id/full/ - view thread and all its post's included via nested serialization

      preview/ - view threads and 3 last posts

    /posts/ - basic posts view

    /search/ - joined post+threads search via drf_multiple_model

      ?tags__name=tags/ - search by tags

      ?body__icontains=content/ - search by text (case-insensitive)

    /images/ - images view

## Directories description:##

    /tests/ - tests and console clients

      /bump_test.py - test thread bumping, deletion (thread limit), and bumplimit  

      /fulfill.py - populate forum with messages via ad hoc scenario

      /rest_methods.py - library, can be used for manual posting

## How this app works:##

+  we assume that regular users are anonymous, only mods/superusers can log-in

+  Threads are sorted by last time something was posted to them

+  Also they have bump-limit parameters which defines when new posts ceases to bump them up

+  every board (forum section) have its upper limit of amount of threads it can have, when it is reached every new thread deletes one from the tail

+  anonymous can view content of forum sections, but not create new / modify old

+  anonymous can post and view threads and posts, but not modify

+  users can post up to 4 images at once, new threads MUST have at least one

+  every post/thread have unique id, users can mention them with >>id references

+  tags can be added to posts/thread with #sharp syntax

## What have been done so far (including "dayum look how i can read DRF manuals"):##

+  models - views - serializers - urls casket

+  models logic

+  miserable angular front-end in separated branch where nobody will see it (i dont really know angular, just watched few vids on youtube)

+  front-end ready url links generation according to models relationships

+  plus links to threads/posts via user's '>>id' references in text

+  same with #tags

+  optional nested serialization (thread preview through @list_route() preview option - check ThreadViewSet)

+  search/filtering

+  pagination

+  writable nested serialization

+  sending images and text in one POST via custom parser

+  basic integration testing

+  basic authentication/permissions/moderation

+  vps + docker + whitenoise

## Currently working on:##

+  testing best practices

+  pep8/clean-up/comments

+  separated local/production settings and other nasty stuff from Two Scoops Of Django  

## To do:##

+  throttling

+  more complicated authentications/permissions system

+  front-end i can be proud of

+  more tests

## Howto:##

+  docker-compose build

+  docker-compose up -d

+ sudo ng build --prod --output-path /some_path --output-hashing none
