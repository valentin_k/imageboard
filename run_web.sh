#!/bin/sh

# wait for PSQL server to start
sleep 5

cd imageboard
# prepare init migration

su -m myuser -c "python manage.py collectstatic --noinput"
su -m myuser -c "python manage.py makemigrations"
# migrate db, so we have the latest db schema
su -m myuser -c "python manage.py migrate"
# start development server on public ip interface, on port 8000
su -m myuser -c "gunicorn imageboard.wsgi:application -b :8000 --reload"
