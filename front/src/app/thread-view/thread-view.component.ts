import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-thread-view',
  templateUrl: './thread-view.component.html',
  styleUrls: ['./thread-view.component.css']
})
export class ThreadViewComponent implements OnInit {

  threadLink:string;
  ip:string = '188.225.36.114';
  thread: any;

  @Input('threadLink')
    public set value(val: string) {
      this.threadLink = val;
      this.load();
    }

  load() {
    this.http.get(this.threadLink).subscribe(data => {
      this.thread = data;
    });
  }

  constructor(private http: HttpClient) {}

  ngOnInit() {
  }

}
