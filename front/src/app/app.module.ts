import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { CarouselModule } from 'ngx-bootstrap/carousel';

import { AppComponent } from './app.component';
import { BoardListComponent } from './board-list/board-list.component';
import { BoardBrowserComponent } from './board-browser/board-browser.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ThreadViewComponent } from './thread-view/thread-view.component';

@NgModule({
  declarations: [
    AppComponent,
    BoardListComponent,
    BoardBrowserComponent,
    MainPageComponent,
    ThreadViewComponent
  ],
  imports: [
    BrowserModule,
    CarouselModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
