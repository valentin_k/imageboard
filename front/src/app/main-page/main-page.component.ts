import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  ip:string = '188.225.36.114';
  pictures: any;

  pictureClicked(index) {
    console.log(index)
  }

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get('http://'+this.ip+'/api/images/').subscribe(data => {
      this.pictures = data['results'];
    });
  }

}
