import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.css'],
})
export class BoardListComponent implements OnInit {

    @Input('init')
    count: number;

    @Output()
    link: EventEmitter<string> = new EventEmitter<string>();

    results: any;

    selection(index) {
      this.link.emit(this.results[index]['links']['threads']);
    }



    ip:string = '188.225.36.114';
    //ip = '127.0.0.1';
    // ip = 'localhost';

    constructor(private http: HttpClient) {}

    ngOnInit(): void {
      this.http.get('http://'+this.ip+'/api/boards/').subscribe(data => {
        // Read the result field from the JSON response.
        this.results = data;
      });
    }

  }
