import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  boardLink:string = 'empty';
  boardPage:number = 0;
  pageState:string = 'main';

  threadLink:string = 'empty';

  myCount: number = 10;
  countChange(event) {
    this.myCount = event;
  }

  reset() {
    this.myCount = 0;
  }

  boardSelected(event) {
    this.pageState = 'refresh';
    this.pageState = 'board';
    this.boardPage = 0;
    this.boardLink = event;
  }

  postSelected(event) {
    this.threadLink = event;
    this.pageState = 'thread';
    console.log(event)
  }

  // boardPicked: string = 'nothing yet';
  //
  // boardChange(event) {
  //     this.boardPicked = event;
  // }
}
