import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardBrowserComponent } from './board-browser.component';

describe('BoardBrowserComponent', () => {
  let component: BoardBrowserComponent;
  let fixture: ComponentFixture<BoardBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
