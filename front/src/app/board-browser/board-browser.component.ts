import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-board-browser',
  templateUrl: './board-browser.component.html',
  styleUrls: ['./board-browser.component.css']
})
export class BoardBrowserComponent implements OnInit {

  boardLink:string;
  ip:string = '188.225.36.114';
  results: any;
  recentPosts: any[];
  threads: any[];
  pageNumber:number = 1;
  pageCount:number = 0;

  @Input('boardLink')
    public set value(val: string) {
      this.boardLink = val;
      this.load();
    }

  @Output()
  postLink: EventEmitter<string> = new EventEmitter<string>();

  load() {
    this.http.get(this.boardLink).subscribe(data => {
      this.results = data;
      this.pageCount = data['page_count']
      this.threads = data['results']
    });
  }

  left() {
    if (this.results['links']['left'] != null)
    {
      this.boardLink = this.results['links']['left'];
      this.pageNumber--;
      this.load();
    }
  }

  right() {
    if (this.results['links']['right'] != null)
    {
      this.boardLink = this.results['links']['right'];
      this.pageNumber++;
      this.load();
    }
  }

  postSelected(index) {
    this.postLink.emit(this.results['results'][index]['links']['self']);
  }

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.load()
  }

}
