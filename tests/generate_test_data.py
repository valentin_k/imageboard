import json
import random

#n of test cases created
n=45

# images are 0.jpg  ... 76.jpg
pf=0
pl=76

random.seed(666)

with open('kushka.txt', 'r') as textfile:
    text=textfile.readlines()
    l=len(text)

    #fragment of text made from 2-32 random strings and 1-4 picturess
    data=[{ 'l':random.randint(2, 32),\
            'images':random.sample(range(pf, pl), random.randint(1,4))} \
            for x in range(n)]
    data = [ dict(x, **{'begin': random.randint(0,l-1-x['l'])}) for x in data[:]]

    with open('posting.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)
