import json
import unittest
import requests


from rest_methods import post_thread, delete_threads, delete_threads,\
    post_message, create_board, delete_board, get_threads


class TestBumping(unittest.TestCase):
    """
    prooves that our bumping system works well
    """

    @classmethod
    def setUpClass(cls):

        r = create_board('t', 'testing', bump_limit = 10, thread_limit = 12)

        with open('posting.json', 'r') as jfile:
            cls.data = json.load(jfile)
            with open('kushka.txt', 'r') as textfile:
                text=textfile.readlines()

                for i in range(12):
                    d = cls.data.pop()
                    body = text[d['begin']:d['begin']+d['l']]
                    r = post_thread(d['images'], '\n'.join(body)[:999], 't')

    @classmethod
    def tearDownClass(cls):
        r = delete_board('t')

    def test_bump(self):
        """
            posting to thread makes it move to the top
        """
        id_control=[x['id'] for x in get_threads()['results']]

        with open('kushka.txt', 'r') as textfile:
            text=textfile.readlines()

            #bump two threads
            d=self.data.pop()
            body=text[d['begin']:d['begin']+d['l']]
            post_message(d['images'],'\n'.join(body)[:999], id_control[3])

            d=self.data.pop()
            body=text[d['begin']:d['begin']+d['l']]
            post_message(d['images'],'\n'.join(body)[:999], id_control[5])

            #check bumping result
            id_bumped=[x['id'] for x in get_threads()['results']]

            #modify control measurment like if it was bumped
            tmp=id_control[3]
            id_control.remove(tmp)
            id_control=[tmp,]+id_control

            tmp=id_control[5]
            id_control.remove(tmp)
            id_control=[tmp,]+id_control

            self.assertEqual(id_bumped, id_control)

    def test_bump_limit(self):
        """
            posting to thread in bump_limit does not makes it move to the top
        """
        id_control=[x['id'] for x in get_threads()['results']]

        with open('kushka.txt', 'r') as textfile:
            text=textfile.readlines()

            #move first thread to bumplimit
            for i in range(15):
                d=self.data.pop()
                body=text[d['begin']:d['begin']+d['l']]
                post_message(d['images'],'\n'.join(body)[:999], id_control[0])

            #bumping second thread to first place
            d=self.data.pop()
            body=text[d['begin']:d['begin']+d['l']]
            post_message(d['images'],'\n'.join(body)[:999], id_control[1])

            #try to bump ex-first (now second) thread to the top
            d=self.data.pop()
            body=text[d['begin']:d['begin']+d['l']]
            post_message(d['images'],'\n'.join(body)[:999], id_control[1])

            #two first have exchanged their places
            id_bumped=[id_control[1],id_control[0]]
            id_bumped+=id_control[2:]

            self.assertEqual(id_bumped, [x['id'] for x in get_threads()['results']])

    def test_thread_limit(self):
        """
            least popular thread ceases to exist after new one was posted
        """
        id_control=[x['id'] for x in get_threads()['results']]

        our_thread=id_control[-1]

        with open('kushka.txt', 'r') as textfile:
            text=textfile.readlines()

            #we have thread_limit=12 so 10+3 is enaugh
            for i in range(3):
                d=self.data.pop()
                body=text[d['begin']:d['begin']+d['l']]
                r=post_thread(d['images'], '\n'.join(body)[:999], 't')

            #try to post to old thread
            d=self.data.pop()
            body=text[d['begin']:d['begin']+d['l']]
            message = post_message(d['images'],'\n'.join(body)[:999], our_thread)

            #400 Bad Request
            self.assertEqual(str(message),'<Response [400]>')


if __name__ == '__main__':

    unittest.main()
