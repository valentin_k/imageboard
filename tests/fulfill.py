import json
import requests

from rest_methods import post_thread, get_threads, post_message


def fulfill(board, scenario):
    with open('posting.json', 'r') as jfile:
        data = json.load(jfile)
        with open('kushka.txt', 'r') as textfile:
            text=textfile.readlines()

            #make threads
            for i in range(len(scenario)):
                d=data.pop()
                body=text[d['begin']:d['begin']+d['l']]
                r=post_thread(d['images'], '\n'.join(body)[:999], board)

            url='http://188.225.36.114/api/threads/?board='+board
            # url='http://127.0.0.1:8000/api/threads/?board='+board
            url+='&page_size='+str(object=len(scenario))

            r = requests.get(url)

            id=[x['id'] for x in r.json()['results']]

            #populate threads
            for i in range(len(scenario)):
                for j in range(scenario[i]):
                    d=data.pop()
                    body=text[d['begin']:d['begin']+d['l']]
                    post_message(d['images'],'\n'.join(body)[:999], id[i])


if __name__ == '__main__':
    #4 thread with some messages in
    fulfill('b', [0,6,2,1][::-1])
