import requests
import pprint
import json

# manage.py createsuperuser
auth=('valentin', 'vpassword')

adress = '188.225.36.114'
# adress = '127.0.0.1'


def post_thread(files,body,board):
    url = 'http://'+adress+'/api/threads/'
    multiple_files = [
            ('images', (str(i)+'.jpg', open('images/'+str(i)+'.jpg', 'rb'), 'image/jpg'))
            for i in files]

    data = {'body':body, 'board':board}

    r = requests.post(url, data = data, files=multiple_files)
    return r

def delete_threads(board='t'):
    url='http://'+adress+'/api/threads/?board='
    while(True):
        response = requests.get(url+board)
        api = response.json()
        if len(api['results'])==0:
            return 204
        for thread in api['results']:
            r2=requests.delete(url+'/'+str(thread['id']))

def get_threads(board='t'):
    url='http://'+adress+'/api/threads/?board='+board

    response = requests.get(url)
    return response.json()

def post_message(files,body,thread):
    url = 'http://'+adress+'/api/posts/'
    multiple_files = [
            ('images', (str(i)+'.jpg', open('images/'+str(i)+'.jpg', 'rb'), 'image/jpg'))
            for i in files]

    data = {'body':body, 'mother_thread':thread}
    r = requests.post(url, data = data, files=multiple_files)
    return r

def create_board(name, description, bump_limit = 10, thread_limit = 15):
    url = 'http://'+adress+'/api/boards/'

    data = {'name':name, 'description':description, 'bump_limit':bump_limit, 'thread_limit':thread_limit}
    r = requests.post(url, data = data, auth=auth)
    return r

def delete_board(name):
    url = 'http://'+adress+'/api/boards/'

    r = requests.delete(url+str(name)+'/', auth=auth)
    return r

if __name__ == '__main__':


    # r=post_thread([2,13,16,22], 'ok here #some #tags and links >>647 >>648', 'b')
    # r=post_thread([1,2,3,4], 'Какой-то текст и #некоторые #теги', 'b')

    # r=post_message([7,8,9], 'некоторый текст со ссылками >>651 на пост и на предыдущие сообщение >>652 ', 651)

    # r=delete_board('b')
    r=create_board('b', 'random')
    print(r.json())
    r=create_board('c', 'programming')
    print(r.json())
    r=create_board('a', 'anime')
    print(r.json())
    # print(r.json())
    # r=delete_board('b')
    # print(r)

    pass
